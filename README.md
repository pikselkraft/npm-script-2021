# NPM scripts workflow

<img src="https://www.pikselkraft.com/media/site/1c7a6b9896-1629294690/pikselkraft-connexion-lowtech.png" width="300">

Created by [Pikselkraft](https://www.pikselkraft.com/).

NPM script demo to optimize CSS and front-end workflow.

## Workflow and technologies

* HTML5 / CSS3
* NPM script
* PostCSS

## Installation

> npm init
> npm install --save-dev

## Resources

* [NPM](https://www.npmjs.com)
* [Why NPM scripts](https://css-tricks.com/why-npm-scripts/)
* [Introduction to NPM script](https://www.freecodecamp.org/news/introduction-to-npm-scripts-1dbb2ae01633/)
* [uncss](https://www.npmjs.com/package/uncss)
* [critical](https://www.npmjs.com/package/critical)